<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;
use \Torann\GeoIP\Location;

class CheckConnexion extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var User
     */
    public $user;

    /**
     * The order instance.
     *
     * @var Location
     */

    public $location;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, Location $location)
    {
        $this->user = $user;
        $this->location = $location;

    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.checkConnexion');
    }
}
