<?php

namespace App\Http\Controllers\Auth;

use App\Browser;
use App\Http\Controllers\Controller;
use App\Ip;
use App\User;
use App\Mail\checkConnexion;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Jenssegers\Agent\Agent;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Mail\CheckBrowser;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }


    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {

        $this->validateLogin($request);
        $user = User::where('email', $request->email)->first();
        if ($user->isBanned()) {
            return redirect()->back()
                ->withInput($request->only($this->username()))
                ->withErrors([
                    "user" => "L'utilisateur est banni",
                ]);
        }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);


        return $this->sendFailedLoginResponse($request);
    }
    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        $this->guard()->logout();

        $request->session()->invalidate();

        return $this->loggedOut($request) ?: redirect('/login');
    }

    protected function hasTooManyLoginAttempts(Request $request)
    {
        return $this->limiter()->tooManyAttempts(
            $this->throttleKey($request), 3
        );
    }

    /**
     * The user has been authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  mixed  $user
     * @return mixed
     */
    protected function authenticated(Request $request, $user)
    {


        $liveIp = \config('ip.secondip');

        //save browser from connexion.
        $agent = new Agent();
        $browserClient = $agent->browser();

        $browser = new Browser;
        $browser->browser = $browserClient;
        $browser->user_id =  $user->id;
        $browser->save();

        //save ip from connexion.
        $ip = new Ip;
        $ip->ip = $liveIp;
        //$request->getClientIp();

        $ip->user_id = $user->id;
        $ip->country =     geoip()->getLocation($liveIp)->currency;

        $ip->save();

        if($ip !== $user->usual_ip && $ip->country !== "EUR") {
            Mail::to($request->user())->send(new checkConnexion($user,  geoip()->getLocation($liveIp)));
        }

        $countBrowser = $user->browsers->where('created_at', '>=', Carbon::now()->subDay())->unique('browser')->count('browser');

        if($countBrowser >= 3 ) {
            //block user
            $ban = $user->ban([
                'expired_at' => Carbon::now()->addDay(1),
            ]);

            $ban->isTemporary();
            Mail::to($request->user())->send(new CheckBrowser($user));

        }
    }

}
