<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Browser extends Model
{
    protected $table = 'browser';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['created_at', 'updated_at'];

}
