<p>
    <p> Hey  {{ $user->name }}, </p>
    <p> Nous avons détecté une connexion anormale sur votre compte avec 3 navigateurs différents en moins de 24h. </p>
    <p> Votre compte a été bloqué temporairement. </p>
    <p> Contactez-nous pour plus d'informations.  </p>
</p>
