<div>
    <p>
    Hey  {{ $user->name }},
    </p>

    <p>
    Il semblerait que quelqu'un ait tenté de se connecter à votre compte depuis un nouveau lieu. S'il ne s'agit pas de vous, nous vous conseillons de changer votre mot de passe le plus vite possible.
    </p>

    <p>
    Adresse IP : {{ $user->usual_ip }} Lieu : {{ $location->country  }} ,  {{ $location->city  }}, {{ $user->state_name  }}, {{ $location->postal_code  }}, {{ $location->contient  }}
    </p>

</div>
