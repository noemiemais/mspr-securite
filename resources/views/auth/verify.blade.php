@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Vérifier votre adresse e-mail') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            {{ __('Un nouveau lien de vérification vient de vous être envoyé par mail ') }}
                        </div>
                    @endif

                    {{ __('Avant de commencer, merci de vérifier votre mail avec la lien de validation ') }}
                    {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('Cliquez ici pour en envoyer un de nouveau') }}</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
